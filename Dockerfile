FROM rust as builder

WORKDIR /test

COPY . .

RUN cargo install --path .

FROM debian:12-slim

COPY --from=builder /usr/local/cargo/bin/test-jemalloc-arenas /

CMD ["/test-jemalloc-arenas"]
